import * as yup from "yup";

export const contactFormSchema = yup.object({
  name: yup
    .string()
    .required("Vous devez fournir un Prénom et un NOM")
    .min(3, "Votre Prénom doit contenir au minimum 3 caractères"),
  email: yup
    .string()
    .required("Vous devez fournir un email")
    .email("L'email saisi n'est pas conforme")
    .test("emailContainsAtSymbol", "L'email saisi n'est pas conforme.", (value) =>
      value ? !!value?.match(/^[a-z0-9-]+@[a-z0-9-]+\.[a-z0-9]{2,5}$/) : true
    ),
  message: yup
    .string()
    .required("Vous devez saisir un message")
    .min(10, "Votre message doit contenir au minimum 10 caractères"),
});
