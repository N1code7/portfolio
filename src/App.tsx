import Header from "./components/layouts/Header";
import Home from "./components/pages/Home";
import Skills from "./components/pages/Skills";
import ProgressBar from "./components/ui/ProgressBar";
import Projects from "./components/pages/Projects";
import Contact from "./components/pages/Contact";

const App = () => {
  //

  return (
    <>
      <Header />
      <ProgressBar />

      <main>
        <Home />
        <Skills />
        <Projects />
        <Contact />
      </main>
    </>
  );
};

export default App;
