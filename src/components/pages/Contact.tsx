import emailjs from "@emailjs/browser";
import { FormEvent, useRef, useState } from "react";
import { ValidationError } from "yup";
import { contactFormSchema } from "../../validations/contactFormSchema";

interface IFormErrors {
  [name: string]: string;
}

const Contact = () => {
  //
  const nameRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);
  const messageRef = useRef<HTMLTextAreaElement>(null);
  const formRef = useRef<HTMLFormElement>(null);
  const [formErrors, setFormErrors] = useState({} as IFormErrors);
  const [submitMessage, setSubmitMessage] = useState({ error: "", success: "" });

  const handleName = () => {
    (nameRef.current?.value.length || 0) < 3
      ? (nameRef.current!.style.borderColor = "red")
      : nameRef.current!.removeAttribute("style");
  };
  const handleEmail = () => {
    (emailRef.current?.value.length || 0) < 3
      ? (emailRef.current!.style.borderColor = "red")
      : emailRef.current!.removeAttribute("style");
  };
  const handleMessage = () => {
    (messageRef.current?.value.length || 0) < 3
      ? (messageRef.current!.style.borderColor = "red")
      : messageRef.current!.removeAttribute("style");
  };

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    let errors = {} as IFormErrors;

    contactFormSchema
      .validate(
        {
          name: nameRef.current!.value,
          email: emailRef.current!.value,
          message: messageRef.current!.value,
        },
        { abortEarly: false }
      )
      .then(() =>
        emailjs
          .sendForm("service_ywqydli", "template_wncw1dz", formRef.current!, "ISvsEz5JfaC2s4bdF")
          .then(() => {
            formRef.current?.reset();
            setSubmitMessage({ success: "Votre formulaire a bien été envoyé ! Merci ", error: "" });
          })
          .catch((err) => {
            console.error(err);
            setSubmitMessage({
              error: "Une erreur est survenue lors de l'envoi de votre message",
              success: "",
            });
          })
      )
      .catch((err: ValidationError) => {
        console.dir(err);
        err.inner.forEach(
          (err: ValidationError) => (errors = { ...errors, [err.path as string]: err.message })
        );
      });

    setTimeout(() => {
      setSubmitMessage({ success: "", error: "" });
    }, 5000);

    setFormErrors(errors);
  };

  return (
    <section id="contact">
      <div className="content contact">
        {submitMessage.success && (
          <div className="submit-message success">{submitMessage.success}</div>
        )}
        {submitMessage.error && <div className="submit-message error">{submitMessage.error}</div>}

        <h2>Contact</h2>

        <form className="form" onSubmit={handleSubmit} ref={formRef}>
          <p>Une question, une idée, un projet ? Contactez-moi !</p>

          <div className="form-row">
            {formErrors.name && <div className="form-error">{formErrors.name}</div>}
            <div className="field">
              <input
                type="text"
                ref={nameRef}
                name="name"
                id="name"
                placeholder="Votre Prénom / NOM"
                onBlur={handleName}
                required
              />
              <label htmlFor="name">Votre Prénom / NOM</label>
            </div>
          </div>
          <div className="form-row">
            {formErrors.email && <div className="form-error">{formErrors.email}</div>}
            <div className="field">
              <input
                type="email"
                ref={emailRef}
                name="email"
                id="email"
                placeholder="Votre email"
                onBlur={handleEmail}
                required
              />
              <label htmlFor="email">Votre email</label>
            </div>
          </div>
          <div className="form-row">
            {formErrors.message && <div className="form-error">{formErrors.message}</div>}
            <div className="field">
              <textarea
                ref={messageRef}
                name="message"
                id="message"
                rows={10}
                placeholder="Votre message"
                onBlur={handleMessage}
                required
              />
              <label htmlFor="message">Votre message</label>
            </div>
          </div>
          <div className="form-row">
            <input type="submit" value="Envoyer" />
          </div>
        </form>
      </div>
    </section>
  );
};

export default Contact;
