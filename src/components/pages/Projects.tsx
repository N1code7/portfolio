import data from "./../../data.json";

interface IData {
  projectName: string;
  projectDesc: string;
  url: string;
  imageName: string;
}

const Projects = () => {
  return (
    <section id="projects">
      <div className="content projects">
        <h2>Projets</h2>
        <div className="cards">
          {data.map((elt: IData, index: number) => (
            <article key={index}>
              <div
                className="image"
                style={{ backgroundImage: `url("/assets/${elt.imageName}")` }}
              ></div>
              <div className="info">
                <a href={elt.url} className="project-name">
                  {elt.projectName}
                </a>
                <div className="project-desc">{elt.projectDesc}</div>
              </div>
            </article>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Projects;
