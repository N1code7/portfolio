import { useEffect, useState } from "react";
import photo from "./../../assets/photo-profile.jpg";

const Home = () => {
  const [mobileDisplay, setMobileDisplay] = useState(false);

  const scrollToNextSection = () => {
    const home = document.querySelector("section:first-of-type");
    window.scrollTo(0, (home as HTMLDivElement)?.offsetHeight);
  };

  useEffect(() => {
    const observer = new ResizeObserver((entries) => {
      entries.forEach(() => {
        window.innerWidth < 700 ? setMobileDisplay(true) : setMobileDisplay(false);
      });
    });

    observer.observe(document.body);
  }, [mobileDisplay]);

  return (
    <section id="home">
      <div className="content home">
        <h1>
          <span>Nicolas Mafféïs,</span>
          <span>Développeur full stack</span>
        </h1>
        <img src={photo} alt="photo de profil" />
        <div className="text">
          <p>Je développe vos projets numériques !</p>
          <p>Exposez vos besoins et nous pourrons concevoir le produit qu'il vous faut.</p>
        </div>
        <div className="buttons">
          <button
            className="btn btn-primary"
            onClick={() => window.scrollTo(0, document.body.scrollHeight)}
          >
            Me contacter
          </button>
          <button className="btn btn-secondary" onClick={() => scrollToNextSection()}>
            Me découvrir
          </button>
          <a
            className="btn btn-secondary"
            style={{ padding: "1rem" }}
            href="https://www.linkedin.com/in/nicolas-maff%C3%A9%C3%AFs-15150b150/"
            target="_blank"
            rel="noopener noreferrer"
          >
            {mobileDisplay ? <span>Me contacter via</span> : undefined}
            <svg
              width="25"
              height="25"
              viewBox="0 0 25 25"
              // fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M22.2222 0C22.9589 0 23.6655 0.292658 24.1864 0.813592C24.7073 1.33453 25 2.04107 25 2.77778V22.2222C25 22.9589 24.7073 23.6655 24.1864 24.1864C23.6655 24.7073 22.9589 25 22.2222 25H2.77778C2.04107 25 1.33453 24.7073 0.813592 24.1864C0.292658 23.6655 0 22.9589 0 22.2222V2.77778C0 2.04107 0.292658 1.33453 0.813592 0.813592C1.33453 0.292658 2.04107 0 2.77778 0H22.2222ZM21.5278 21.5278V14.1667C21.5278 12.9658 21.0507 11.8142 20.2016 10.965C19.3525 10.1159 18.2008 9.63889 17 9.63889C15.8194 9.63889 14.4444 10.3611 13.7778 11.4444V9.90278H9.90278V21.5278H13.7778V14.6806C13.7778 13.6111 14.6389 12.7361 15.7083 12.7361C16.224 12.7361 16.7186 12.941 17.0833 13.3056C17.4479 13.6703 17.6528 14.1649 17.6528 14.6806V21.5278H21.5278ZM5.38889 7.72222C6.00773 7.72222 6.60122 7.47639 7.0388 7.0388C7.47639 6.60122 7.72222 6.00773 7.72222 5.38889C7.72222 4.09722 6.68056 3.04167 5.38889 3.04167C4.76637 3.04167 4.16934 3.28896 3.72915 3.72915C3.28896 4.16934 3.04167 4.76637 3.04167 5.38889C3.04167 6.68056 4.09722 7.72222 5.38889 7.72222ZM7.31944 21.5278V9.90278H3.47222V21.5278H7.31944Z"
                // fill="#F1F1F1"
              />
            </svg>
          </a>
        </div>
        <div className="toScroll" onClick={() => scrollToNextSection()}>
          <div className="arrows">
            <svg
              id="firstArrow"
              width="55"
              height="37"
              viewBox="0 0 55 37"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M27.8564 26.4418L6.71285 3L3 6.34884L27.8564 33.9071L52.7128 6.34884L49 3L27.8564 26.4418Z"
                stroke="#00364D"
                strokeWidth="3"
              />
            </svg>
            <svg
              id="secondArrow"
              width="55"
              height="37"
              viewBox="0 0 55 37"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M27.8564 26.4418L6.71285 3L3 6.34884L27.8564 33.9071L52.7128 6.34884L49 3L27.8564 26.4418Z"
                stroke="#00364D"
                strokeWidth="3"
              />
            </svg>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Home;
