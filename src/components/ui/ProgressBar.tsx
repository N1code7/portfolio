import { useEffect, useRef } from "react";

const ProgressBar = () => {
  const progressBarRef = useRef<SVGSVGElement>(null);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (
        window.scrollY >
        document.querySelector("#skills")!.getBoundingClientRect().height - 0.1 * window.innerHeight
      ) {
        progressBarRef.current!.children[1].setAttribute("fill", "#F1F1F1");
      } else {
        progressBarRef.current!.children[1].setAttribute("fill", "none");
      }

      if (
        window.scrollY >
        document.querySelector("#skills")!.getBoundingClientRect().height +
          document.querySelector("#projects")!.getBoundingClientRect().height -
          0.1 * window.innerHeight
      ) {
        progressBarRef.current!.children[3].setAttribute("fill", "#F1F1F1");
      } else {
        progressBarRef.current!.children[3].setAttribute("fill", "none");
      }

      if (
        window.scrollY >
        document.querySelector("#skills")!.getBoundingClientRect().height +
          document.querySelector("#projects")!.getBoundingClientRect().height +
          document.querySelector("#contact")!.getBoundingClientRect().height -
          0.1 * window.innerHeight
      ) {
        progressBarRef.current!.children[5].setAttribute("fill", "#F1F1F1");
      } else {
        progressBarRef.current!.children[5].setAttribute("fill", "none");
      }
    });
  }, []);

  return (
    <aside className="progress-bar">
      <svg
        ref={progressBarRef}
        width="10"
        height="420"
        viewBox="0 0 10 420"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <line x1="5" y1="4.37114e-08" x2="5" y2="80" stroke="#F1F1F1" strokeWidth="2" />
        <path
          d="M9 101C9 103.209 7.20914 105 5 105C2.79086 105 1 103.209 1 101C1 98.7909 2.79086 97 5 97C7.20914 97 9 98.7909 9 101Z"
          stroke="#F1F1F1"
          strokeWidth="2"
        />
        <line x1="5" y1="122" x2="4.99999" y2="237" stroke="#F1F1F1" strokeWidth="2" />
        <path
          d="M9 258C9 260.209 7.20914 262 5 262C2.79086 262 1 260.209 1 258C1 255.791 2.79086 254 5 254C7.20914 254 9 255.791 9 258Z"
          stroke="#F1F1F1"
          strokeWidth="2"
        />
        <line x1="5" y1="279" x2="4.99999" y2="394" stroke="#F1F1F1" strokeWidth="2" />
        <path
          d="M9 415C9 417.209 7.20914 419 5 419C2.79086 419 1 417.209 1 415C1 412.791 2.79086 411 5 411C7.20914 411 9 412.791 9 415Z"
          stroke="#F1F1F1"
          strokeWidth="2"
        />
      </svg>
    </aside>
  );
};

export default ProgressBar;
